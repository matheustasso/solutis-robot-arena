# John Wick's Robot

Não toque no cachorro do robô!

## Descrição:

Robô desenvolvido em Java com o principal foco de find target and fire!

Ponto Forte: O robô se porta bem em localizar o alvo e disparar assim que o mesmo entrar no radar.

Ponto Fraco: Movimentação um pouco devagar, para agregar no quesito localização do alvo.

## O que mais gostei de fazer:

Todo o projeto pra mim foi ótimo, nunca tinha usado a ferramenta e foi horas de diversão fazendo testes com o robô. 

Além de ter a liberdade de apelidar de John Wick, a melhor parte de todo o processo ( ha ha ha).

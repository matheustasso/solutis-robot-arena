package johnwick_robot;
import robocode.*;
import java.awt.Color;

// API help : https://robocode.sourceforge.io/docs/robocode/robocode/Robot.html

/**
 * Johnwick - a robot by Matheus Tasso
 */
public class Johnwick extends Robot
{
    /**
     * run: Johnwick's default behavior
     */
    public void run() {
       
        setColors(Color.blue,Color.red,Color.green); // body,gun,radar
        
        while(true){
    		ahead(100); 
		    turnGunRight(360); 
		    back(75); 
		    turnGunRight(360); 
		
		    //For each second the robot go ahead 25 pixels.
		}
    }

    /**
     * onScannedRobot: What to do when you see another robot
     */

		public void onScannedRobot(ScannedRobotEvent e){
		    double distance = e.getDistance(); 
		    if(distance > 800) //this conditions adjust the fire force according the distance of the scanned robot.
		        fire(5);
		    else if(distance > 600 && distance <= 800)
		        fire(4);
		    else if(distance > 400 && distance <= 600)
		        fire(3);
		    else if(distance > 200 && distance <= 400)
		        fire(2);
		    else if(distance < 200)
		        fire(1);
		}

    /**
     * onHitByBullet: What to do when you're hit by a bullet
     */
    public void onHitByBullet(HitByBulletEvent e) {
        double energy = getEnergy();
    	double bearing = e.getBearing();
    	if(energy < 100){ 
        	turnRight(-bearing); 
        	ahead(100); 
    	}
    	else {
        	turnRight(360); // scan
		}
    }

    /**
     * onHitWall: What to do when you hit a wall
     */
    public void onHitWall(HitWallEvent e) {
        // Replace the next line with any behavior you would like
		double bearing = e.getBearing(); 
    	turnRight(-bearing); 
    	ahead(100); 
    }   
}
